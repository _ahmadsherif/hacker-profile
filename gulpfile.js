// Load plugins
const { series, parallel, src, dest, task } = require('gulp');
const rename = require('gulp-rename');

task('build',
  parallel(
    bulmaswatch, bulmajs, fontawesome_css, fontawesome_font, particles
));
task('build').description = 'Build npm dependencies';

function bulmaswatch() {
  return src('node_modules/bulmaswatch/superhero/bulmaswatch.min.css')
      .pipe(rename('bulmaswatch.css'))
      .pipe(dest('source/stylesheets/'));
};

function bulmajs() {
  const base = 'node_modules/@vizuaalog/bulmajs/dist/';
  return src([ base+'tabs.js', base+'navbar.js', base+'panelTabs.js' ])
      .pipe(dest('source/javascripts/'));
};

function fontawesome_css() {
  return src('node_modules/@fortawesome/fontawesome-free/css/all.css')
      .pipe(dest('source/stylesheets/fontawesome/css/'));
};

function fontawesome_font() {
  return src('node_modules/@fortawesome/fontawesome-free/webfonts/*',
      { base: 'node_modules/@fortawesome/fontawesome-free/'})
      .pipe(dest('source/stylesheets/fontawesome/'));
};

function particles() {
  return src('node_modules/particles.js/particles.js')
      .pipe(rename('particlesjs.js'))
      .pipe(dest('source/javascripts/'));
};

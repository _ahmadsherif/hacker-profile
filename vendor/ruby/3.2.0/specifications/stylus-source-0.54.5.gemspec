# -*- encoding: utf-8 -*-
# stub: stylus-source 0.54.5 ruby lib

Gem::Specification.new do |s|
  s.name = "stylus-source".freeze
  s.version = "0.54.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["TJ Holowaychuk".freeze]
  s.date = "2016-10-25"
  s.description = "Robust, expressive, and feature-rich CSS superset. This gem packages up stylus for use with the stylus gem.".freeze
  s.email = ["tj@vision-media.ca".freeze]
  s.homepage = "https://github.com/forgecrafted/ruby-stylus-source".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.4.6".freeze
  s.summary = "JS source code for Stylus.js".freeze

  s.installed_by_version = "3.4.6" if s.respond_to? :installed_by_version
end

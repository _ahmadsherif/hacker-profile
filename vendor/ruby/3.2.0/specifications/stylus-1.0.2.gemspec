# -*- encoding: utf-8 -*-
# stub: stylus 1.0.2 ruby lib

Gem::Specification.new do |s|
  s.name = "stylus".freeze
  s.version = "1.0.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Lucas Mazza".freeze]
  s.date = "2016-10-25"
  s.description = "Bridge library to compile .styl stylesheets from ruby code.".freeze
  s.email = ["luc4smazza@gmail.com".freeze]
  s.homepage = "https://github.com/lucasmazza/ruby-stylus".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.4.6".freeze
  s.summary = "Ruby Stylus Compiler".freeze

  s.installed_by_version = "3.4.6" if s.respond_to? :installed_by_version

  s.specification_version = 4

  s.add_runtime_dependency(%q<execjs>.freeze, [">= 0"])
  s.add_runtime_dependency(%q<stylus-source>.freeze, [">= 0"])
  s.add_development_dependency(%q<coveralls>.freeze, [">= 0.8.0"])
end

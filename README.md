Install dependencies:

```
bundle install
npm i
```

Test with local web server:

```
$ bundle exec middleman server
```

Build:

```
$ gulp build
$ bundle exec middleman build
```